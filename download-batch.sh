#!/bin/bash

if (($# == 0)); then
  echo "arguments 
  -u {username}
  -p {password}
  -l {storage location}
  -s (default|reprocess)"
fi

behavior="default"
username="username"
password="password"
store_path="c:/videos/"

while getopts ":s:u:p:l:" opt; do
  case $opt in
    s)
      behavior=$OPTARG
      ;;
    u)
      username=$OPTARG
      ;;
    p)
	  password=$OPTARG
	  ;;
    l)
	  store_path=$OPTARG
	  ;;
    \?)
      ;;
    :)
      ;;
  esac
done



complete_log="complete.log"
error_log="error.log"
url_list="url.list" 

if [ $behavior = "reprocess" ]; then
	url_list=${error_log}
fi


template_format="youtube-dl -u ${username} -p ${password} -f best -o '${store_path}%(playlist)s/%(chapter_number)s - %(chapter)s/%(playlist_index)s - %(title)s.%(ext)s'"

# load the url list

# get the url content
read -d $'\x04' names < "${url_list}"

IFS=$'\n' names=(${names}) 

for (( i=0; i<${#names[@]}; i++ ))
do
	echo "# processing ${names[$i]}"
	url_source=${names[$i]}
	command="${template_format} ${url_source}"

	echo "# ${command}"

	# execute the command
	bash -c $command

	if [ $? -eq 0 ]; then
		# if success then put the url in complete log
		echo "${names[$i]}" >> ${complete_log}
	else
		# if not success then put on the error log
		echo "${names[$i]}" >> ${error_log}
	fi

	# remove the url from url list
	url_escape=$(echo "${url_source}" | sed -e 's/[\/&]/\\&/g')
	sed -i -E "/${url_escape}/d" ${url_list}

	sleep 1

done
